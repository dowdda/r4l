package com.hack.rewardsforlife.r4l;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Bazinga on 05/08/15.
 */
public class ReceiptFragment extends Fragment {
    private static View rootView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_reciept, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is  */
        }


        //View view = inflater.inflate(R.layout.fragment_reciept, container, false);
        return rootView;
    }
}
