package com.hack.rewardsforlife.r4l;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Bazinga on 05/08/15.
 */
public class HistoryFragment extends Fragment {
    private static View view;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      if (view == null) {
          view = inflater.inflate(R.layout.fragment_history, container, false);
      }
        return view;
    }
}
