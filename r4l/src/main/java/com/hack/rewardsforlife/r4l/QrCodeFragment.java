package com.hack.rewardsforlife.r4l;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.os.Bundle;
import android.app.Activity;

/**
 * Created by Bazinga on 03/08/15.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class QrCodeFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        final Button generate = (Button) view.findViewById(R.id.btnGenerateCode);

        generate.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView code = (TextView) view.findViewById(R.id.code);
                code.setText(String.valueOf("ReWRD4L"));
            }
        });

        return view;
    }


}
